<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('abcd/test/{id}',function ($id){
    $service=new \App\Services\AmazonService();
    return $service->getCustomerReviewAndRating($id);
    /*$csv = array_map( 'str_getcsv', file(public_path('images/new_keyword.csv')));
    foreach ($csv as $row){
        $kw=\App\Models\KeywordNew::firstOrNew(['url'=>$row[0]]);
        $kw->url=$row[0];
        $kw->save();
    }*/
});
Route::get('/{slug?}','FrontController@getHome')->name('index');
Route::get('category/{slug}','FrontController@getCategory')->name('category');
Route::get('page/{page}','FrontController@getPage')->name('page');