<?php
/**
 * Created by PhpStorm.
 * User: hardikvegad
 * Date: 20/11/18
 * Time: 12:54 AM
 */

namespace App\Http\Controllers;


use App\Models\Category;
use App\Services\ProductService;

class FrontController extends Controller
{
    /**
     * @var ProductService
     */
    private $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function getHome($slug=null)
    {
        if ($slug!=null){
            $post=$this->productService->getListDetail($slug);
            if ($post) {
                $related = $this->productService->getLists(["limit" => 10]);
                return view('post-detail', ['list' => $post, 'related_lists' => $related]);
            }
            else
            {
                return redirect()->route('index');
            }
        }
        else{
            $lists=$this->productService->getLists(['paginate'=>"yes"]);
            return view('welcome',['lists'=>$lists]);
        }
    }

    public function getCategory($slug)
    {
        $category=Category::where('slug',$slug)->first();
        if ($category){
            $lists=$this->productService->getLists(['category_id'=>$category->id,'paginate'=>'yes']);
            return view('category',['category'=>$category,'lists'=>$lists]);
        }
        else{
            return redirect()->route('index');
        }
    }

    public function getPage($page)
    {
        if ($page=="about-us")
            return view('about-us');
        elseif ($page=="privacy-policy")
            return view('privacy-policy');
        elseif ($page=="cookie-policy")
            return view('cookie-policy');
        elseif ($page=="affiliate-disclosure")
            return view('affiliate-disclosure');
        elseif ($page=="test")
            return view('test');
    }
}