<?php
/**
 * Created by PhpStorm.
 * User: hardikvegad
 * Date: 25/11/18
 * Time: 12:07 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class KeywordNew extends Model
{
    protected $table="keywords_new";
    protected $fillable=['keyword','category_id','url'];
}