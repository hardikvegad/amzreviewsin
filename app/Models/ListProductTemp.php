<?php
/**
 * Created by PhpStorm.
 * User: hardikvegad
 * Date: 14/09/16
 * Time: 6:22 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ListProductTemp extends Model
{
    protected $table="lists_product_temp";
    protected $fillable=['list_id','product_id'];

}