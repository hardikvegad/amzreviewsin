<?php
/**
 * Created by PhpStorm.
 * User: hardikvegad
 * Date: 06/09/16
 * Time: 8:12 PM
 */

namespace App\Models;


use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use Sluggable;
    protected $table="categories";

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'category_name'
            ]
        ];
    }
}