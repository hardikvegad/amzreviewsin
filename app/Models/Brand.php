<?php
/**
 * Created by PhpStorm.
 * User: hardikvegad
 * Date: 06/09/16
 * Time: 8:17 PM
 */

namespace App\Models;


use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table="brands";
    protected $fillable=array('brand_name');
    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'brand_name'
            ]
        ];
    }
}