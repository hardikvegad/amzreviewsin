<?php
/**
 * Created by PhpStorm.
 * User: hardikvegad
 * Date: 12/09/16
 * Time: 11:46 AM
 */

namespace App\Models;


use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Lists extends Model
{
    use Sluggable;
    protected $table = "lists";

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}