<?php
/**
 * Created by PhpStorm.
 * User: hardikvegad
 * Date: 06/09/16
 * Time: 7:53 PM
 */

namespace App\Models;


use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable=array('product_id');
    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}