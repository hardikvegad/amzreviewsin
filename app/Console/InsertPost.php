<?php
/**
 * Created by PhpStorm.
 * User: hardikvegad
 * Date: 22/09/16
 * Time: 11:18 PM
 */

namespace App\Console;


use App\Models\Campaign;
use App\Models\Keyword;
use App\Models\KeywordNew;
use App\Models\User;
use App\Services\AmazonService;
use App\Services\CallService;
use App\Services\ChatService;
use App\Services\ContactService;
use App\Services\LeadService;
use App\Services\MailService;
use App\Services\ProductService;
use App\Services\TwilioService;
use App\Services\UserService;
use Carbon\Carbon;
use Illuminate\Console\Command;

class InsertPost extends Command
{
    protected $signature = 'cron:insert-post';

    protected $description = 'Insert Post';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $amazon_service=new AmazonService();
        $product_service=new ProductService();
        $keyword=\DB::table('keywords')->select("*")->where('status',0)->inRandomOrder()->limit(1)->first();
        if ($keyword){
            if ($product_service->insertPost($keyword->keyword,$keyword->category_id)){
                $key=Keyword::find($keyword->id);
                $key->keyword=$keyword->keyword;
                $key->category_id=$keyword->category_id;
                $key->status=1;
                $key->save();
                echo "<pre>";
                print_r(['success']);
            }
        }
    }
}