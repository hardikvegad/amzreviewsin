<?php
/**
 * Created by PhpStorm.
 * User: hardikvegad
 * Date: 22/09/16
 * Time: 11:18 PM
 */

namespace App\Console;


use App\Models\Campaign;
use App\Models\Keyword;
use App\Models\User;
use App\Services\AmazonService;
use App\Services\CallService;
use App\Services\ChatService;
use App\Services\ContactService;
use App\Services\LeadService;
use App\Services\MailService;
use App\Services\ProductService;
use App\Services\TwilioService;
use App\Services\UserService;
use Carbon\Carbon;
use Illuminate\Console\Command;

class Sitemap extends Command
{
    protected $signature = 'cron:sitemap';

    protected $description = 'Insert Post';

    public function __construct()
    {
        parent::__construct();
    }

    function Submit($url){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return $httpCode;
    }

    public function handle()
    {
        $product_service=new ProductService();
        $product_service->generateXml();
        $this->Submit("http://www.google.com/webmasters/sitemaps/ping?sitemap=https://toptenz.co/sitemap.xml");
    }
}