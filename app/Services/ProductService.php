<?php
/**
 * Created by PhpStorm.
 * User: hardikvegad
 * Date: 18/11/18
 * Time: 3:22 PM
 */

namespace App\Services;


use App\Models\ListProduct;
use App\Models\ListProductTemp;
use App\Models\Lists;
use App\Models\Product;

class ProductService
{
    public function saveProduct($data)
    {
        $product=Product::firstOrCreate(['product_id'=>$data['product_id']]);
        $product->title=html_entity_decode($data['title']);
        $product->description=html_entity_decode($data['description']);
        $product->product_id=$data['product_id'];
        $product->url="";
        $product->meta_title="";
        $product->meta_description="";
        $product->meta_keyword="";
        $product->rating=$data['rating'];
        $product->reviews=str_replace(",","",$data['review']);
        $product->affiliate_link=$data['affiliate_link'];
        $product->thumb_image=$data['thumb_image'];
        $product->image=$data['image'];
        $product->track_code=$data['track_code'];
        $product->features=$data['feature'];
        $product->sale_price=$data['sale_price'];
        $product->original_price=$data['price'];
        $product->sales_rank=$data['sales_rank'];
        $product->brand_name=$data['brand'];
        $product->save();
        return $product;
    }

    public function saveList($data)
    {
        if (isset($data['list_id']))
            $list=Lists::find($data['list_id']);
        else
            $list=new Lists();
        if (isset($data['title']))
            $list->title=$data['title'];
        if (isset($data['slug']))
            $list->slug=$data['slug'];
        if (isset($data['post_content']))
            $list->post_content=$data['post_content'];
        if (isset($data['rating_value']))
            $list->rating_value=$data['rating_value'];
        if (isset($data['rating_count']))
            $list->rating_count=$data['rating_count'];
        if (isset($data['featured_image']))
            $list->featured_image=$data['featured_image'];
        if (isset($data['meta_title']))
            $list->meta_title=$data['meta_title'];
        if (isset($data['meta_description']))
            $list->meta_description=$data['meta_description'];
        if (isset($data['json_item_list_meta']))
            $list->json_item_list_meta=$data['json_item_list_meta'];
        if (isset($data['status']))
            $list->status=$data['status'];
        if (isset($data['large_image']))
            $list->large_image=$data['large_image'];
        if (isset($data['meta_list']))
            $list->meta_list=$data['meta_list'];
        if (isset($data['keyword']))
            $list->keyword=$data['keyword'];
        if (isset($data['category_id']))
            $list->category_id=$data['category_id'];
        $list->save();
        return $list;
    }

    public function saveProductList($list_id,$product_id,$score)
    {
        $list = new ListProduct();
        $list->list_id = $list_id;
        $list->product_id = $product_id;
        $list->product_score = $score;
        $list->save();
    }

    public function saveTempProductList($list_id,$product_id,$score)
    {
        $list =ListProductTemp::firstOrCreate(['list_id'=>$list_id,'product_id'=>$product_id]);
        $list->list_id = $list_id;
        $list->product_id = $product_id;
        $list->product_score = $score;
        $list->save();
    }

    public function getTempProductList($list_id)
    {
        $result=\DB::table("lists_product_temp")
            ->select("lists_product_temp.product_id","products.title","products.image","products.thumb_image")
            ->join("products",'products.id','=','lists_product_temp.product_id')
            ->where("lists_product_temp.list_id",$list_id)
            ->orderBy("reviews","DESC")->orderBy("sales_rank","ASC")
            ->limit(10)->get();
        return $result;
    }

    public function getListProducts($list_id)
    {
        $product=\DB::table('lists_product')
            ->select('lists_product.*','lists_product.product_score','lists_product.product_id','products.*')
            ->join('products','products.id','=','lists_product.product_id')
            ->where('lists_product.list_id',$list_id)
            ->orderBy('product_score','desc')
            ->get();
        return $product;
    }

    public function getLists($data)
    {
        $category_id=isset($data['category_id'])?$data['category_id']:0;
        $limit=isset($data['limit'])?$data['limit']:0;
        $paginate=isset($data['paginate'])?$data['paginate']:0;
        $search=isset($data['search'])?$data['search']:'';
        $result=\DB::table("lists")
            ->select("lists.slug","lists.meta_title","lists.created_at","lists.featured_image","categories.category_name",'categories.slug as category_slug')
            ->leftJoin("categories","categories.id",'=','lists.category_id')
            ->where('lists.status',1);
            if ($category_id!=0)
                $result->where('categories.id',$category_id);
            if ($search!="")
                $result->where('keyword','like',"%{$search}%");
            if ($limit)
                return $result->orderBy("lists.created_at","desc")->limit($limit)->get();
            if ($paginate)
                return $result->orderBy("lists.created_at","desc")->paginate(24);
            $result=$result->get();
        return $result;
    }

    public function getListDetail($slug)
    {
        return \DB::table("lists")
            ->select("lists.*","categories.slug as category_slug","categories.category_name")
            ->leftJoin("categories",'categories.id','=','lists.category_id')
            ->where('lists.slug',$slug)->first();
    }

    public function templates($keyword)
    {
        $titles=[" In India Buyer’s Guide"," In India Our Top Picks","In India To Buy In","In India Reviews","In India Available In"];
        $title=$titles[rand(0,4)];
        $main_paragraph=[
                "If you are ready to choose a new [keyword], check out our recommendations for the [keyword]. But if you’d like to learn more about the various types of [keyword] available and how to choose the right one for you, read on.",
                "If you are considering to buy a [keyword], you should go through the following [keyword].",
                "Check out our product review of the [keyword]. We tried 10 models and can tell you the absolute best varieties.",
                "Are you looking for the Top 10 [keyword]? Well, just grab a cup of coffee and read on! I am a huge fan of [keyword]",
                "Reading in bed is a common behavior, and for some individuals, it is one of the things that they love doing in their free time. so here is Top 10 [keyword].",
                "Looking For The [keyword]? Have We Got Something For You!",
                "Looking for some of the [keyword]? As you are already aware of the fact that the market is filled with many [keyword]. Also, all the products come with a different price tag. Hence it becomes quite confusing for us to choose the best one. However just to help you out, we have handpicked the top [keyword]. So let’s just check them out.",
                "Are you a [keyword] enthusiast? If so then you may want to read the list of top 10 [keyword].",
                "Looking For a Top [keyword] That Can Last and Perform Well? Read Our Review and Buying Guide on [keyword].",
                "Here you have my list of best [keyword], all very different in price, style, and design, so you have plenty of options to choose. Please keep reading.",
                "You might be planning on getting [keyword]. Everyone loves a product which has an invaluable investment. Check out the best [keyword] out there. List of Top 10 [keyword].",
                "This guide comes with the top 10 [keyword] that are available on the market. Therefore, if you want the [keyword] for the same purpose, then you have everything here and the option to choose from the best variety. Buyers Guide When choosing the [keyword].",
                "If you are looking for top quality [keyword], read the detailed reviews on top 10 [keyword] before buying. Click here to read more.",
                "Here, we’ll review the top 10 [keyword] on the market today. We’ll provide an overview of what is good and not-so-good about each case, and we’ve provided a range of economic and excellent [keyword].",
                "Below is a top 10 [keyword] review to guide you buy the best product for you. Just choose one of these products and you are guaranteed of long service",
                "Buy the [keyword] for your needs. Choose from the [keyword] on the market. Pick the one that satisfies your need.",
                "To choose [keyword]. The issue arises when choosing the [keyword] from the unending variety that exists in our markets. Don’t worry; we got our hands on 10 trendy and top rated products which have been approved for various materials.",
                "In this review, we have reviewed the [keyword] available today. We have also included a comprehensive guide that helps you pick the most appropriate model. Additionally, going through the top 10 [keyword] reviews will help you narrow down your choice of the most appropriate choice.",
                "The choice of a [keyword] is determined by the ease and convenience of use, your yard type and some personal preferences. One thing is certain – there is always a perfect [keyword] for everyone. However, in-depth analysis of the types of [keyword] is inevitable to make a choice you are going to be satisfied with. We’ve made the major part ourselves and sharing our observation here in this detailed of Top 10 [keyword].",
                "Getting the right one in a market with various options is an uphill task. That’s why we examined a brands to come up with this list of the top 10 [keyword]. Having undergone thorough testing, we hope that you’re going to find one of them impressive.",
                "Everybody wants to get the best services without spending much money. However, it is not easy to choose a [keyword] if you have not bought one in years. But there is no need for you to worry since we have compiled the perfect list of Top 10 [keyword] for you. Have a look at our list and choose only the best one.",
                "In this article, you will discover top 10 [keyword] available in the market with expert reviews. Get any one of them and have fun! You’ll love it!",
                "[keyword] are one of those things that seem like they’re probably all the same. To help you choose the [keyword] for your job, we’ve put together this comprehensive guide for [keyword]. It’s not going to consider absolutely every possible material combination or bonding factor, but we will try and cover as much as possible.",
                "With our [keyword] guide and product reviews, we’ll help you make a more informed decision and get a product with the benefits you need. It’s important to do your prior research, so read our reviews and comparisons to decide which model you need."
        ];
        $description=$main_paragraph[rand(0,23)];
        return ['title'=>$keyword.' '.$title,'description'=>str_replace("[keyword]",$keyword,$description)];
    }

    public function insertPost($keyword,$category_id)
    {
        $service=new AmazonService();
        $result=$service->getFullResult($keyword);
        if (count($result)>0){
            try {
                $list = $this->saveList(['title' => "Best " . $keyword.' In India', 'keyword' => $keyword,'category_id'=>$category_id]);
                $image = "";
                $large_image = "";
                $cnt = 1;
                foreach ($result as $row) {
                    if ($row['thumb_image'] != "" || $row['image'] != "") {
                        $product = $this->saveProduct($row);
                        if ($cnt == 1) {
                            $image = $row['thumb_image'];
                            $large_image = $row['image'];
                        }
                        $this->saveTempProductList($list->id, $product->id, 0);
                        $cnt++;
                    }
                }
                $list_product = $this->getTempProductList($list->id);
                $score = [9.8, 9.4, 9.1, 8.8, 8.5, 8.2, 7.7, 7.4, 7.1, 6.8];
                $cnt = 0;
                $json = [];
                foreach ($list_product as $row) {
                    $title = $row->title;
                    if (strlen($row->title) > 100) {
                        $tt = explode("-", $row->title);
                        $title = $tt[0];
                    }
                    $this->saveProductList($list->id, $row->product_id, $score[$cnt]);
                    $json[] = [
                        "@type" => "ListItem",
                        "position" => $cnt + 1,
                        "item" => ["@type" => "Product", "url" => route("index", $list->slug . "#" . \Illuminate\Support\Str::slug($title)), "name" => $title, "image" => $row->thumb_image]
                    ];
                    $cnt++;
                }
                $json = json_encode(["@context" => "http://schema.org", "@type" => "ItemList", "numberOfItems" => 10, "itemListElement" => $json]);
                $metas = $this->templates("Best ".$keyword);
                $product_list = $this->getListProducts($list->id);
                $html = view("generate-html", ['list' => $list, 'products' => $product_list, 'meta' => $metas]);
                if ($html!=null) {
                    $this->saveList(['list_id' => $list->id, 'post_content' => $html, 'featured_image' => $image, 'meta_title' => $metas['title'], 'meta_description' => $metas['description'], 'large_image' => $large_image, 'meta_list' => $json, 'status' => 1]);
                    return true;
                }
                else
                    return false;
            }catch (\Exception $e){
                \Log::info("Error".$e->getMessage());
                return false;
            }
        }
        return false;
    }

    public function generateXml()
    {
        $list = Lists::all();
        $fp = fopen(public_path("sitemap.xml"), "w");
        fwrite($fp, '<?xml version="1.0" encoding="UTF-8"?>');
        fwrite($fp, '<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xmlns:video="http://www.google.com/schemas/sitemap-video/1.1" xmlns:geo="http://www.google.com/geo/schemas/sitemap/1.0" xmlns:news="http://www.google.com/schemas/sitemap-news/0.9" xmlns:mobile="http://www.google.com/schemas/sitemap-mobile/1.0" xmlns:pagemap="http://www.google.com/schemas/sitemap-pagemap/1.0" xmlns:xhtml="http://www.w3.org/1999/xhtml">');
        $str = "<url><loc>https://toptenz.co</loc><lastmod>" . date('c', time()) . "</lastmod><changefreq>always</changefreq><priority>1.0</priority></url>";
        fwrite($fp, $str);
        if (count($list) > 0) {
            foreach ($list as $row) {
                $str = "<url><loc>https://toptenz.co/" . $row->slug . "</loc><lastmod>" . date('c', time()) . "</lastmod><changefreq>weekly</changefreq><priority>0.6</priority></url>";
                fwrite($fp, $str);
            }
        }
        fwrite($fp, "</urlset>");
        fclose($fp);
    }
}