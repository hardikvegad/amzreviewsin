<?php
/**
 * Created by PhpStorm.
 * User: hardikvegad
 * Date: 26/09/16
 * Time: 1:38 PM
 */

namespace App\Services;


use App\Models\Keyword;
use Sunra\PhpSimple\HtmlDomParser;

class AmazonService
{
    const RETURN_TYPE_ARRAY	= 1;
    const RETURN_TYPE_OBJECT = 2;

    private $protocol='SOAP';
    private $requestConfig = array();
    private $responseConfig = array(
        'returnType'			=> self::RETURN_TYPE_ARRAY,
        'responseGroup'			=> 'Small',
        'optionalParameters'	=> array()
    );
    private $possibleLocations = array('de', 'com', 'co.uk', 'ca', 'fr', 'co.jp', 'it', 'cn', 'es', 'in');
    protected $webserviceWsdl = 'http://webservices.amazon.com/AWSECommerceService/AWSECommerceService.wsdl';

    protected $webserviceEndpoint = 'https://webservices.amazon.%%COUNTRY%%/onca/%%PROTOCOL%%?Service=AWSECommerceService';
    private $xmlAmazonLink = null;
    private $the_plugin = null;
    private $amz_settings = array();

    public function __construct()
    {

        // private setter helper
        $this->setProtocol();

        $this->requestConfig['accessKey']		 = "AKIAJOYGETEIB7OTLDWA";
        $this->requestConfig['secretKey']		 = "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";
        $this->associateTag("9codein-21");
        $this->country("in");
    }
    private function setProtocol()
    {
        $this->protocol = 'SOAP';
    }
    public function search($pattern,$page=1)
    {
        $params = $this->buildRequestParams('ItemSearch', array_merge(
            array(
                'Keywords'      => $pattern,
                'SearchIndex'   => "All",
                'ItemPage'      => $page
            )
        ));
        $asin=array();
        $data=$this->returnData($this->performTheRequest("ItemSearch", $params));
        if(isset($data['Items']) && $data['Items']['Request']['IsValid']=="True"){
            if(count($data['Items']['Item'])>0){
                foreach($data['Items']['Item'] as $row){
                    $asin[]=$row['ASIN'];
                }
            }
        }
        return $asin;
    }

    public function lookup($asin)
    {
        $this->responseGroup('Large,OfferSummary');
        $params = $this->buildRequestParams('ItemLookup', array(
            'ItemId' => $asin,
        ));
        $response=array();
        $data=$this->returnData($this->performTheRequest("ItemLookup", $params));
        if(isset($data['Items']) && $data['Items']['Request']['IsValid']=="True"){
            if(isset($data['Items']['Item'])){
                $item=$data['Items']['Item'];
                $response['affiliate_link']=$item['DetailPageURL'];
                $response['product_id']=$asin;
                $response['title']=$item['ItemAttributes']['Title'];
                $temp=explode(",",$response['title']);
                $response['title']=$temp[0];
                if(isset($item['ItemAttributes']['Feature']))
                    if(is_array($item['ItemAttributes']['Feature']))
                        $response['feature']=implode("~~",$item['ItemAttributes']['Feature']);
                    else
                        $response['feature']="";
                else
                    $response['feature']="";
                if(isset($item['ItemAttributes']['Brand']))
                    $response['brand']=$item['ItemAttributes']['Brand'];
                else
                    $response['brand']="";
                $response['track_code']=md5($asin);
                if(isset($item['LargeImage']))
                    $response['image']=$item['LargeImage']['URL'];
                else
                    $response['image']="";
                if(isset($item['MediumImage']))
                    $response['thumb_image']=$item['MediumImage']['URL'];
                else
                    $response['thumb_image']="";
                if(isset($item['Offers']['Offer']['OfferListing']['Price']['Amount']))
                    $response['price']=number_format((float) ($item['Offers']['Offer']['OfferListing']['Price']['Amount'] / 100), 2, '.', '');
                else
                    $response['price']=0.0;
                if(isset($item['OfferSummary']['LowestNewPrice']['Amount']))
                    $response['sale_price']=number_format((float) ($item['OfferSummary']['LowestNewPrice']['Amount'] / 100), 2, '.', '');
                else
                    $response['sale_price']=0;
                if(isset($item['EditorialReviews']['EditorialReview']['Content']))
                    $response['description']=strip_tags($item['EditorialReviews']['EditorialReview']['Content']);
                else
                    $response['description']="";
                if (isset($item['SalesRank']))
                    $response['sales_rank']=$item['SalesRank'];
                else
                    $response['sales_rank']='';
                $r=$this->getCustomerReviewAndRating($asin);
                $response['review']=$r['review'];
                $response['rating']=$r['rating'];
            }
        }
        return $response;
    }

    protected function buildRequestParams($function, array $params)
    {
        $associateTag = array();

        if(false === empty($this->requestConfig['associateTag']))
        {
            $associateTag = array('AssociateTag' => $this->requestConfig['associateTag']);
        }

        return array_merge(
            $associateTag,
            array(
                'AWSAccessKeyId' => $this->requestConfig['accessKey'],
                'Request' => array_merge(
                    array('Operation' => $function),
                    $params,
                    $this->responseConfig['optionalParameters'],
                    array('ResponseGroup' => $this->prepareResponseGroup())
            )));
    }

    protected function prepareResponseGroup()
    {
        if (false === strstr($this->responseConfig['responseGroup'], ','))
            return $this->responseConfig['responseGroup'];

        return explode(',', $this->responseConfig['responseGroup']);
    }

    private function aws_signed_request($region, $params, $public_key, $private_key, $associate_tag=NULL, $version='2011-08-01') {
        // some paramters
        $method = 'GET';
        $host = 'webservices.amazon.'.$region;
        $uri = '/onca/xml';

        // additional parameters
        $params['Service'] = 'AWSECommerceService';
        $params['AWSAccessKeyId'] = $public_key;
        // GMT timestamp
        $params['Timestamp'] = gmdate('Y-m-d\TH:i:s\Z');
        // API version
        $params['Version'] = $version;
        if ($associate_tag !== NULL) {
            $params['AssociateTag'] = $associate_tag;
        }

        // sort the parameters
        ksort($params);

        // create the canonicalized query
        $canonicalized_query = array();
        foreach ($params as $param=>$value)
        {
            $param = str_replace('%7E', '~', rawurlencode($param));
            $value = str_replace('%7E', '~', rawurlencode($value));
            $canonicalized_query[] = $param.'='.$value;
        }
        $canonicalized_query = implode('&', $canonicalized_query);

        // create the string to sign
        $string_to_sign = $method."\n".$host."\n".$uri."\n".$canonicalized_query;

        // calculate HMAC with SHA256 and base64-encoding
        $signature = base64_encode(hash_hmac('sha256', $string_to_sign, $private_key, TRUE));

        // encode the signature for the request
        $signature = str_replace('%7E', '~', rawurlencode($signature));

        // create request
        $request = 'http://'.$host.$uri.'?'.$canonicalized_query.'&Signature='.$signature;

        return $request;
    }

    protected function performSoapRequest($function, $params)
    {
        $this->webserviceEndpoint = str_replace(
            '%%PROTOCOL%%',
            strtolower( $this->protocol ),
            $this->webserviceEndpoint
        );

        $soapClient = new \SoapClient(
            $this->webserviceWsdl,
            array('exceptions' => 1)
        );

        $soapClient->__setLocation(str_replace(
            '%%COUNTRY%%',
            $this->responseConfig['country'],
            $this->webserviceEndpoint
        ));

        $soapClient->__setSoapHeaders($this->buildSoapHeader($function));

        //var_dump('<pre>',$soapClient->__soapCall($function, array($params)),'</pre>'); die;
        return $soapClient->__soapCall($function, array($params));
    }

    protected function buildSoapHeader($function)
    {
        $timeStamp = $this->getTimestamp();
        $signature = $this->buildSignature($function . $timeStamp);

        return array(
            new \SoapHeader(
                'http://security.amazonaws.com/doc/2007-01-01/',
                'AWSAccessKeyId',
                $this->requestConfig['accessKey']
            ),
            new \SoapHeader(
                'http://security.amazonaws.com/doc/2007-01-01/',
                'Timestamp',
                $timeStamp
            ),
            new \SoapHeader(
                'http://security.amazonaws.com/doc/2007-01-01/',
                'Signature',
                $signature
            )
        );
    }


    protected function performTheRequest($function, $params)
    {
        $ret = null;

        // verify amazon request rate (per second)
        //$this->verify_amazon_request_rate();


        if( $this->protocol == 'SOAP' ) {
            //echo 'soap';
            $ret = $this->returnData(
                $this->performSoapRequest($function, $params)
            );
        }

        // save amazon request time
        //$this->save_amazon_request_time();

        return $ret;
    }
    final protected function getTimestamp()
    {
        return gmdate("Y-m-d\TH:i:s\Z");
    }

    /**
     * provides the signature
     *
     * @return string
     */
    final protected function buildSignature($request)
    {

        return base64_encode(hash_hmac("sha256", $request, $this->requestConfig['secretKey'], true));
    }

    /**
     * Basic validation of the nodeId
     *
     * @param integer $nodeId
     *
     * @return boolean
     */
    final protected function validateNodeId($nodeId)
    {
        //if (false === is_numeric($nodeId) || $nodeId <= 0)
        if (false === is_numeric($nodeId))
        {
            throw new InvalidArgumentException(sprintf('Node has to be a positive Integer.'));
        }

        return true;
    }

    /**
     * Returns the response either as Array or Array/Object
     *
     * @param object $object
     *
     * @return mixed
     */
    protected function returnData($object)
    {
        switch ($this->responseConfig['returnType'])
        {
            case self::RETURN_TYPE_OBJECT:
                return $object;
                break;

            case self::RETURN_TYPE_ARRAY:
                return $this->objectToArray($object);
                break;

            default:
                throw new InvalidArgumentException(sprintf(
                    "Unknwon return type %s", $this->responseConfig['returnType']
                ));
                break;
        }
    }

    /**
     * Transforms the responseobject to an array
     *
     * @param object $object
     *
     * @return array An arrayrepresentation of the given object
     */
    protected function objectToArray($object)
    {
        $out = array();
        foreach ($object as $key => $value)
        {
            switch (true)
            {
                case is_object($value):
                    $out[$key] = $this->objectToArray($value);
                    break;

                case is_array($value):
                    $out[$key] = $this->objectToArray($value);
                    break;

                default:
                    $out[$key] = $value;
                    break;
            }
        }

        return $out;
    }

    /**
     * set or get optional parameters
     *
     * if the argument params is null it will reutrn the current parameters,
     * otherwise it will set the params and return itself.
     *
     * @param array $params the optional parameters
     *
     * @return array|aaAmazonWS depends on params argument
     */
    public function optionalParameters($params = null)
    {
        if (null === $params)
        {
            return $this->responseConfig['optionalParameters'];
        }

        if (false === is_array($params))
        {
            throw new InvalidArgumentException(sprintf(
                "%s is no valid parameter: Use an array with Key => Value Pairs", $params
            ));
        }

        $this->responseConfig['optionalParameters'] = $params;

        return $this;
    }

    /**
     * Set or get the country
     *
     * if the country argument is null it will return the current
     * country, otherwise it will set the country and return itself.
     *
     * @param string|null $country
     *
     * @return string|aaAmazonWS depends on country argument
     */
    public function country($country = null)
    {
        if (null === $country)
        {
            return $this->responseConfig['country'];
        }

        if (false === in_array(strtolower($country), $this->possibleLocations))
        {
            throw new InvalidArgumentException(sprintf(
                "Invalid Country-Code: %s! Possible Country-Codes: %s",
                $country,
                implode(', ', $this->possibleLocations)
            ));
        }

        $this->responseConfig['country'] = strtolower($country);

        return $this;
    }

    /**
     * Setting/Getting the amazon category
     *
     * @param string $category
     *
     * @return string|aaAmazonWS depends on category argument
     */
    public function category($category = null)
    {
        if (null === $category)
        {
            return isset($this->requestConfig['category']) ? $this->requestConfig['category'] : null;
        }

        $this->requestConfig['category'] = $category;

        return $this;
    }

    /**
     * Setting/Getting the responsegroup
     *
     * @param string $responseGroup Comma separated groups
     *
     * @return string|aaAmazonWS depends on responseGroup argument
     */
    public function responseGroup($responseGroup = null)
    {
        if (null === $responseGroup)
        {
            return $this->responseConfig['responseGroup'];
        }

        $this->responseConfig['responseGroup'] = $responseGroup;

        return $this;
    }

    /**
     * Setting/Getting the returntype
     * It can be an object or an array
     *
     * @param integer $type Use the constants RETURN_TYPE_ARRAY or RETURN_TYPE_OBJECT
     *
     * @return integer|aaAmazonWS depends on type argument
     */
    public function returnType($type = null)
    {
        if (null === $type)
        {
            return $this->responseConfig['returnType'];
        }

        $this->responseConfig['returnType'] = $type;

        return $this;
    }

    /**
     * Setter/Getter of the AssociateTag.
     * This could be used for late bindings of this attribute
     *
     * @param string $associateTag
     *
     * @return string|aaAmazonWS depends on associateTag argument
     */
    public function associateTag($associateTag = null)
    {
        if (null === $associateTag)
        {
            return $this->requestConfig['associateTag'];
        }

        $this->requestConfig['associateTag'] = $associateTag;

        return $this;
    }

    /**
     * @deprecated use returnType() instead
     */
    public function setReturnType($type)
    {
        return $this->returnType($type);
    }

    /**
     * Setting the resultpage to a specified value.
     * Allows to browse resultsets which have more than one page.
     *
     * @param integer $page
     *
     * @return aaAmazonWS
     */
    public function page($page)
    {
        if (false === is_numeric($page) || $page <= 0)
        {
            throw new InvalidArgumentException(sprintf(
                '%s is an invalid page value. It has to be numeric and positive',
                $page
            ));
        }

        $this->responseConfig['optionalParameters'] = array_merge(
            $this->responseConfig['optionalParameters'],
            array("ItemPage" => $page)
        );

        return $this;
    }

    public function getCode($url)
    {
        $proxies=['168.90.199.49:80','104.144.6.49:80','107.172.49.70:80','168.90.199.197:80','173.44.225.232:80','173.44.225.72:80','107.172.53.105:80','181.214.212.66:80','104.144.112.59:80','181.214.213.101:80'];
        $proxy=$proxies[rand(0,9)];
        $ch=curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.111 Safari/537.36");
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate,sdch');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($ch, CURLOPT_PROXY, $proxy);
        curl_setopt($ch, CURLOPT_PROXYUSERPWD,"wegasoft:Kavya37356");
        curl_setopt($ch, CURLOPT_REFERER, 'https://www.google.com/');
        $output = curl_exec($ch);
        curl_close($ch);
        $html=HtmlDomParser::str_get_html($output);
        return $html;
    }

    public function getCustomerReviewAndRating($asin)
    {
        $rating = 0;
        $review = 0;
        $url = "https://www.amazon.in/product-reviews/" . $asin;
        //$url="https://www.amazon.com/dp/".$asin;

        $html = $this->getCode($url);
        if ($html)
        {
            if (count($html->find("div[id=cm_cr-product_info]"))) {
                $div = $html->find("div[id=cm_cr-product_info]", 0);
                if (count($div->find("div[class=a-fixed-left-grid]"))) {
                    $temp = trim($div->find("div[class=a-fixed-left-grid]", 0)->plaintext);
                    if (strpos($temp, "stars")) {
                        $temp = explode("stars", $temp);
                        $review = trim($temp[1]);
                        $temp1 = explode("out", $temp[0]);
                        $rating = trim($temp1[0]);
                    }
                }
            }
            /*if (count($html->find("span[id=acrCustomerReviewText]"))) {
                $temp=explode(" ",$html->find("span[id=acrCustomerReviewText]",0)->plaintext);
                $temp=str_replace(",","",trim($temp[0]));
                $review=$temp;
            }*/
        }
        return ['rating'=>$rating,'review'=>$review];
    }

    public function getNumber($str){
        $str = preg_replace('/\D/', '', $str);
        return $str;
    }

    public function getFullResult($keyword)
    {
        $result=array();$cnt=0;
        for($i=1;$i<=2;$i++) {
            $data = $this->search($keyword,$i);
            foreach ($data as $row) {
                if ($cnt==4){
                    sleep(2);
                    $cnt=0;
                }
                $d = $this->lookup($row);
                $result[]=$d;
                $cnt++;
            }
        }
        return $result;
    }

    public function getXml($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.2; rv:28.0) Gecko/20100101 Firefox/28.0');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    public function getXmlData() {
        $xml = $this->getXml("https://comparison-sitemaps.s3.amazonaws.com/best_reviews-US/sitemap.xml");
        $xml=simplexml_load_string($xml);
        $cnt=1;$counter2=0;
        foreach ($xml->url as $row){
            $keyword=new Keyword();
            $keyword->url=$row->loc;
            $keyword->save();
        }
    }

    public function getKeywords($url)
    {
        $category = [
            'electronics-and-computers' => 1,
            "toys-kids-and-baby" => 2,
            "beauty-health-and-grocery" => 3,
            "automotive-and-industrial" => 4,
            "clothing-shoes-and-jewelry" => 5,
            "movies-music-and-games" => 6,
            "books-and-audible" => 7,
            "home-garden-and-tools" => 8,
            "sports-and-outdoors" => 9,
        ];

        $html = $this->getCode($url);
        $word="";$category_id=0;
        if ($html) {
            if ($html->find('ol[class=breadcrumbs]')) {
                $ol = $html->find('ol[class=breadcrumbs]', 0);
                $slug = '';
                $keyword = '';
                foreach ($ol->find('a[itemprop=item]') as $key => $row) {
                    if ($key == 1) {
                        if (preg_match('/topics/', $row->href)) {
                            if ($row->href) {
                                $slug = str_replace('/topics/', '', $row->href);
                            }

                        }else{
                            $keyword = trim($row->plaintext);
                        }
                    }
                    if ($key == 2) {
                        $keyword = trim($row->plaintext);
                    }

                }

                if($slug!=''){
                    $category_id=$category[$slug];
                }
                $word=$keyword;
            }
        }
        return ['keyword'=>$word,'category_id'=>$category_id];
    }
}