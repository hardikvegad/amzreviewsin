@extends('layout')
@section('meta')
    <title>Compare and Buy The Best Products Online In India - AmzReviews</title>
    <link rel="canonical" href="{{ route('index') }}"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="Compare and Buy The Best Products Online - AmzReviews"/>
    <meta property="og:url" content="{{ route('index') }}"/>
    <meta property="og:site_name" content="AmzReviews"/>
    <meta property="article:section" content="Home"/>
@stop
@section('content')
    <div class="main-box vce-related-box">
        <h3 class="main-box-title">Latest Reviews</h3>
        <div class="main-box-inside">
            @if(count($lists)>0)
                @foreach($lists as $row)
                    <article class="vce-post vce-lay-d post type-post status-publish format-standard has-post-thumbnail hentry" style="height: 100px;">
                        <div class="meta-image">
                            <a href="/{{ $row->slug }}" title="{{ $row->meta_title }}">
                                <img width="145" height="100" src="{{ $row->featured_image }}" class="attachment-vce-lay-d size-vce-lay-d wp-post-image" alt="{{ $row->meta_title }}">
                            </a>
                        </div>
                        <header class="entry-header">
                            <span class="meta-category">@if($row->category_name!="")<a href="{{ route('category',$row->category_slug) }}" class="category-36">{{ $row->category_name }}</a>@endif</span>
                            <h2 class="entry-title"><a href="/{{ $row->slug }}" title="{{ $row->meta_title }}">Top 10 {{ $row->meta_title.' '.date('Y') }}</a></h2>
                        </header>
                    </article>
                @endforeach
                <nav id="vce-pagination">
                    @if($lists->lastPage()>1)
                        @if($lists->currentPage()!=1)
                            <a href="{{ $lists->url($lists->currentPage()-1) }}" >Previous</a>
                        @endif
                        @if($lists->currentPage() != $lists->lastPage())
                            <a href="{{ $lists->url($lists->currentPage()+1) }}" >Next</a>
                        @endif
                    @endif
                </nav>
            @endif
        </div>
    </div>
@stop