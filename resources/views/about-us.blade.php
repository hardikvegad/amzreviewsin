@extends('layout')
@section('meta')
    <title>About US - AmzReviews</title>
@stop
@section('content')
    <main id="main" class="main-box main-box-single">
        <article class="vce-single post type-post status-publish format-standard has-post-thumbnail hentry">
            <header class="entry-header">
                <h1 class="entry-title">About Us</h1>
            </header>
            <div class="entry-content">
                <p>AmzReviews is a website with full of up-to-date information, which can benefit you for daily life.At AmzReviews, we publish product reviews list, health advice as well as other useful information you want to know.</p>
                <p>Here at AmzReviews, we believe all readers on the internet deserve to read all the high-quality articles which really can solve their problem and need very fast. That’s why we keep providing only worth reading and worth sharing articles/tips/review on our website</p>
            </div>
        </article>
    </main>
@stop