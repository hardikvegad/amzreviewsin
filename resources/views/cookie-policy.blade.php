@extends('layout')
@section('meta')
    <title>Cookie Policy - AmzReviews</title>
@stop
@section('content')
    <main id="main" class="main-box main-box-single">
        <article class="vce-single post type-post status-publish format-standard has-post-thumbnail hentry">
            <header class="entry-header">
                <h1 class="entry-title">Cookie Policy</h1>
            </header>
            <div class="entry-content ">
                <h2>What Are Cookies?</h2>
                <p>As is common practice with almost all professional websites this site uses cookies, which are tiny files that are downloaded to your computer, to improve your experience. This page describes what information they gather, how we use it and why we sometimes need to store these cookies. We will also share how you can prevent these cookies from being stored however this may downgrade or ‘break’ certain elements of the sites functionality.</p>
                <p>For more general information about cookies, see the&nbsp;website&nbsp;AllAboutCookies.org.</p>
                <h3>How We Use Cookies</h3>
                <p>We use cookies for a variety of reasons detailed below. Unfortunately in most cases there are no industry standard options for disabling cookies without completely disabling the functionality and features they add to this site. It is recommended that you leave on all cookies if you are not sure whether you need them or not in case they are used to provide a service that you use.</p>
                <h3>Disabling Cookies</h3>
                <p>You can prevent the setting of cookies by adjusting the settings on your browser. Be aware that disabling cookies will affect the functionality of this and many other websites that you visit. Disabling cookies will usually result in also disabling certain functionality and features of this site. Therefore it is recommended that you do not disable cookies.</p>
                <p>Please see information how to prevent new cookies from being installed and how to delete existing cookies. The exact procedure depends on which browser you are using.</p>
                <p><strong>Internet Explorer</strong><br>
                    To prevent new cookies from being installed and delete existing cookies:<br>
                    http://windows.microsoft.com/en-GB/internet-explorer/delete-manage-cookies</p>
                <p><strong>Firefox</strong><br>
                    To prevent new cookies from being installed:<br>
                    https://support.mozilla.org/en-US/kb/enable-and-disable-cookies-website-preferences</p>
                <p><strong>To delete existing cookies:</strong><br>
                    https://support.mozilla.org/en-US/kb/delete-cookies-remove-info-websites-stored</p>
                <p><strong>Google Chrome</strong><br>
                    To prevent new cookies from being installed and delete existing cookies:<br>
                    https://support.google.com/chrome/answer/95647?hl=en</p>
                <p><strong>Safari</strong><br>
                    To prevent new cookies from being installed and delete existing cookies:<br>
                    http://help.apple.com/safari/mac/8.0/#/sfri11471</p>
                <p><strong>Microsoft Edge</strong><br>
                    To prevent new cookies from being installed and delete existing cookies:<br>
                    https://www.computerhope.com/issues/ch000509.htm#edge</p>
                <h2>Type of cookies we use on our website</h2>
                <p><strong>Necessary</strong></p>
                <p>These cookies are essential to enable you to move around the website and use its features. Without these cookies, we cannot provide some of the basic functionalities of our website. These technical cookies do not require consent of the user.</p>
                <p><strong>Performance</strong></p>
                <p>These cookies generally collect information about how visitors use our website, for instance which pages visitors go to most often, and the pages that they don’t. This helps us to understand and improve the site, so it is easy to use and includes helpful content. They also allow us to fix bugs or glitches on the website. These cookies don’t collect information that identifies visitors, so we can’t identify you individually.</p>
                <p><strong>Functionality</strong></p>
                <p>These cookies allow our website to remember the choices you make as you browse the site. They provide more enhanced and personal features. The information collected is anonymised and they cannot track your browsing activity on other sites once you leave our site.</p>
                <p><strong>Marketing/ Advertising</strong></p>
                <p>We use these cookies to optimize communication, analyze information on the geographical location of visitors and their use of the site to deliver content, newsletter or email that is more relevant to you and your interests or subscription. The information obtained is anonymous and not related to your personal data.</p>
                <p>If you want information about the exact cookies we use in our site just drop us a line, we will answer as soon as possible.</p>
                <h2>Third Party Cookies</h2>
                <p>In some special cases we also use cookies provided by trusted third parties. The following section details which third party cookies you might encounter through this site.</p>
                <ul class="list-l1">
                    <li>This site uses Google Analytics which is one of the most widespread and trusted analytics solutions on the web for helping us to understand how you use the site and ways that we can improve your experience. These cookies may track things such as how long you spend on the site and the pages that you visit so we can continue to produce engaging content.</li>
                </ul>
                <p>For more information on Google Analytics cookies, see the official Google Analytics page.</p>
                <ul class="list-l1">
                    <li>Third party analytics are used to track and measure usage of this site so that we can continue to produce engaging content. These cookies may track things such as how long you spend on the site or pages you visit which help us to understand how we can improve the site for you.</li>
                    <li>From time to time we test new features and make subtle changes to the way that the site is delivered. When we are still testing new features these cookies may be used to ensure that you receive a consistent experience whilst on the site whilst ensuring we understand which optimisations our users appreciate the most.</li>
                    <li>As we review and refer products it’s important for us to understand statistics about how many of the visitors to our site actually make a purchase and as such this is the kind of data that these cookies will track.</li>
                    <li>We use adverts to offset the costs of running this site and provide funding for further development. The behavioral advertising cookies used by this site are designed to ensure that we provide you with the most relevant adverts where possible by anonymously tracking your interests and presenting similar things that may be of interest.</li>
                    <li>We also use social media buttons and/or plugins on this site that allow you to connect with your social network in various ways. For these to work the following social media sites including Facebook, Twitter, YouTube, Instagram, Yandex, Bing and Webtrends, will set register your personal data and set cookies through our site which may be used to enhance your profile on their site or contribute to the data they hold for various purposes outlined in their respective privacy policies.</li>
                </ul>
            </div>
        </article>
    </main>
@stop