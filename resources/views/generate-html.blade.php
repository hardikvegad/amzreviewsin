<div class="entry-content">
    <p>
        {{ $meta['description'] }}
    </p>
    <p>
        We spent {{ rand(30,50) }} hours to find a best option for you is a <a href="{{ $products[0]->affiliate_link }}" target="_blank" rel="nofollow">{{ $products[0]->title }}</a>,  which comes with amazing features you’ve never heard before.
        It is the {{ $list->title }} available in the market today. However, if you don’t want to spend big on {{ str_replace("Best","",$list->title) }}, then you should absolutely go for <a href="{{ $products[1]->affiliate_link }}" target="_blank" rel="nofollow">{{ $products[1]->title }}</a> which comes with all the basic features one could expect in {{ str_replace("Best","",$list->title) }}.
    </p>
    <p>
        To help you in your search, we have completed this list of the {{ $list->title }}.
    </p>
    <?php
        $score=[
            ['svg'=>'M 26.15082846608263 3.541798620555447 A 39 39 0 1 0 40 1','span'=>'<span>9. </span>7'],
            ['svg'=>'M 21.69060905135026 5.565043878501847 A 39 39 0 1 0 40 1','span'=>'<span>9. </span>5'],
            ['svg'=>'M 13.702206884234766 11.200241715590554 A 39 39 0 1 0 40 1','span'=>'<span>9. </span>1'],
            ['svg'=>'M 8.771476532999419 16.638079657892114 A 39 39 0 1 0 40 1','span'=>'<span>8. </span>8'],
            ['svg'=>'M 6.089697749296462 20.736267203214986 A 39 39 0 1 0 40 1','span'=>'<span>8. </span>6'],
            ['svg'=>'M 2.364356715579845 29.7746220330063 A 39 39 0 1 0 40 1','span'=>'<span>8. </span>2'],
            ['svg'=>'M 1.3795453190787583 34.57224906255745 A 39 39 0 1 0 40 1','span'=>'<span>8. </span>0</span>'],
            ['svg'=>'M 1.0465606436840744 41.90514102202894 A 39 39 0 1 0 40 1','span'=>'<span>7 .</span>7</span>'],
            ['svg'=>'M 2.093520977436704 49.170542411001044 A 39 39 0 1 0 40 1','span'=>'<span>7. </span>4</span>'],
            ['svg'=>'M 3.54179862055544 53.84917153391735 A 39 39 0 1 0 40 1','span'=>'<span>7. </span>2</span>']
        ];
    ?>
    <h2 itemprop="name" style="text-align:center">Top 10 {{ $list->title }} {{ date('Y') }}</h2>
    <?php $cnt=0;$cnt1=1; ?>
    @if(count($products)>0)
        <div class="stnd_tbl_area">
            <table class="stnd-table " style="font-size: 13pt; width: 100%;">
                <thead style="background-color: #f0f1f4; color: #3d5274;">
                <tr class="stnd-table-header" style="height: 44px;">
                    <th data-index="1" class="stnd-th item_row_def unsorted" colspan="1" style="width: 10%;">
                        <h5 class="stnd-th-title">Rank</h5>
                    </th>
                    <th data-index="3" class="stnd-th item_row_def unsorted" style="width: 10%;"></th>
                    <th class="rf_hidden-xs" style="width: 10%">
                        <h5 class="stnd-th-title">Manufacturer</h5>
                    </th>
                    <th data-index="4" class="stnd-th item_row_def unsorted" colspan="1" style="width: 31%;">
                        <h5 class="stnd-th-title">Product Name</h5>
                    </th>
                    <th id="sort-score" class="cmprb_tooltip-container sorted-score sorted-calm stnd-th item_row_def sorted sorted_DESC" style="width: 10%;" colspan="1" data-message-h1="The Score is the fastest way to find your ideal product." data-message-h2="The Score aggregates:" data-message-details="Popularity, Price, Customer reviews, Brand reputation &amp; Expert articles." data-index="5">
                        <h5 class="stnd-th-title">
                            <span class="stht-text"><span class="stht-text-inner">Score</span></span>
                        </h5>
                        <span class="arrow" style="display: inline;">
                            <img width="22" height="12" alt="arrow" src="/images/arrow_th.png">
                        </span>
                    </th>
                    <th data-index="9" class="hideElementLower767 stnd-th item_row_def unsorted" colspan="1" style="width: 15%;"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $row)
                    <tr class="cmprb_list__product-partial">
                        <td class="stnd-td srp-cell srp-img-cell centered" style="position: relative;">
                            <div class="number">
                                <span class="number-value">{{ $cnt1 }}</span>
                            </div>
                            @if($cnt1==1)
                                <div class="cmprb_ribbon cmprb_ribbon--top-choice">
                                    <img class="cmprb_ribbon__image" src="/images/top-choice.svg">
                                </div>
                            @elseif($cnt1==3)
                                <div class="cmprb_ribbon cmprb_ribbon--best-value">
                                    <img class="cmprb_ribbon__image" src="/images/best-value.svg">
                                </div>
                            @endif
                        </td>
                        <td class="image-list-td" style="text-align:center;position:relative;">
                            <a target="_blank" href="{{ $row->affiliate_link }}" rel="nofollow">
                                <img alt="{{ $row->title }}" class="image-list-tag" src="{{ $row->thumb_image }}" style="max-height: 110px; max-width: 130px; left: 5px; top: 10px; z-index: 0;vertical-align:top;" title="{{ $row->title }}">
                            </a>
                        </td>
                        <td class="rf_hidden-xs stnd-td srp-cell srp-brand-cell">
                            {{ $row->brand_name }}
                        </td>
                        <td class="stnd-td srp-cell srp-title-cell" style="font-size: 16px; position: relative;">
                            <div class="product-name-td">
                                <h3 class="srp-listing-name" style="color:#e2234d;">
                                    <a class="switchurl" target="_blank" href="{{ $row->affiliate_link }}"><span>{{ $row->title }}</span></a>
                                </h3>
                                <div class="stan_buy_name" style="line-height: 20px;margin-top: 2px;">
                                    <span style="font-size:14px;">
                                        By {{ $row->brand_name }}
                                    </span>
                                </div>
                            </div>
                        </td>
                        <td class="stnd-td srp-cell" style="text-align: center;position: relative;">
                            <div class="canvas score-circle">
                                <svg class="pie circle-value-rating circle-active">
                                    <path data-size="78" fill="none" style="stroke:rgb(51, 84, 102);" d="{{ $score[$cnt]['svg'] }}"></path>
                                </svg>
                                <span class="count score_count list_score"><?php echo $score[$cnt]['span'];?></span>
                            </div>
                            <div class="cb_mobile__button">
                                <a  class="clickout cb_mobile__button--cta" target="_blank" rel="nofollow" href="{{ $row->affiliate_link }}"><i class="fa fa-amazon"></i> Buy Now</a>
                            </div>
                        </td>
                        <td class="srp-button hideElementLower767" style="text-align: right">
                            <div class="button-block">
                                <div class="compare-drop-holder">
                                    <div class="get-deal">
                                        <a class="btn-sample view-product-btn view-product-btn--default" target="_blank"  href="{{ $row->affiliate_link }}"><i class="fa fa-amazon"></i> Check Price at Amazon</a>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <?php $cnt++;$cnt1++ ?>
                @endforeach
                </tbody>
            </table>
        </div>
    @endif
</div>