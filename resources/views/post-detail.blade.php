@extends('layout')
@section('meta')
    <title>Top 10 {{ $list->meta_title.' '.date("Y") }} - AmzReviews</title>
    <link rel="canonical" href="{{ route('index',$list->slug) }}"/>
    <meta name="description" content="{{ $list->meta_description }}"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="{{ $list->meta_title.' '.date("Y") }} - AmzReviews"/>
    <meta property="og:description" content="{{ $list->meta_description }}"/>
    <meta property="og:url" content="{{ route('index',$list->slug) }}"/>
    <meta property="og:site_name" content="AmzReviews"/>
    <meta property="article:published_time" content="{{ date("Y-m-DTH:i:s",strtotime($list->created_at)).'+00:00' }}"/>
    <meta property="og:image" content="{{ $list->large_image }}"/>
    <meta property="og:image:secure_url" content="{{ $list->large_image }}"/>
    <meta property="og:image:width" content="1000"/>
    <meta property="og:image:height" content="1000"/>
    <meta name="twitter:card" content="summary_large_image"/>
    <meta name="twitter:title" content="{{ $list->meta_title.' '.date("Y") }} - AmzReviews"/>
    <meta name="twitter:image" content="{{ $list->large_image }}"/>
@stop
@section('page-css')
    <link rel='stylesheet' href='{{ asset('css/style-extra.css') }}' type='text/css'/>
@stop
@section('page-js')
    <script type="application/ld+json">
        <?php echo $list->meta_list;?>
    </script>
@stop
@section('content')
    <main id="main" class="main-box main-box-single">
        <article class="vce-single post type-post status-publish format-standard has-post-thumbnail hentry">
            <header class="entry-header">
                <span class="meta-category">
                    @if($list->category_name!="")
                        <a href="{{ route('category',$list->category_slug) }}" class="category-36">{{ $list->category_name }}</a>
                    @endif
                </span>
                <h1 class="entry-title">Top 10 {{ $list->meta_title.' '.date('Y') }}</h1>
                <div class="entry-meta"><div class="meta-item date"><span class="updated">{{ $list->created_at }}</span></div></div>
            </header>
            <?php echo $list->post_content; ?>
        </article>
    </main>
    <div class="main-box vce-related-box">
        <h3 class="main-box-title">You may also like</h3>
        <div class="main-box-inside">
            @if(count($related_lists)>0)
                @foreach($related_lists as $row)
                    <article class="vce-post vce-lay-d post type-post status-publish format-standard has-post-thumbnail hentry" style="height: 100px;">
                        <div class="meta-image">
                            <a href="{{ route('index',$row->slug) }}" title="{{ $row->meta_title }}">
                                <img width="145" height="100" src="{{ $row->featured_image }}" class="attachment-vce-lay-d size-vce-lay-d wp-post-image" alt="{{ $row->meta_title }}">
                            </a>
                        </div>
                        <header class="entry-header">
                            <span class="meta-category">@if($row->category_name!="")<a href="{{ route('category',$row->category_slug) }}" class="category-36">{{ $row->category_name }}</a>@endif</span>
                            <h2 class="entry-title"><a href="/{{ $row->slug }}" title="{{ $row->meta_title.' '.date('Y') }}">Top 10 {{ $row->meta_title.' '.date('Y') }}</a></h2>
                        </header>
                    </article>
                @endforeach
            @endif
        </div>
    </div>
@stop