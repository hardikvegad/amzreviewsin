<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie8"><![endif]-->
<!--[if IE 9]>
<html class="ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en-US" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="user-scalable=yes, width=device-width, initial-scale=1.0, maximum-scale=1, minimum-scale=1">
    <!--[if lt IE 9]>
    <script src="https://toptenz.co/wp-content/themes/voice/js/html5.js"></script>
    <![endif]-->
    @yield('meta')
    <link rel='dns-prefetch' href='//fonts.googleapis.com'/>
    <link rel='dns-prefetch' href='//s.w.org'/>
    <link rel='stylesheet' id='vce_font_0-css' href='https://fonts.googleapis.com/css?family=Poppins%3A400&#038;subset=latin%2Clatin-ext&#038;ver=2.7' type='text/css' media='screen'/>
    <link rel='stylesheet' id='vce_style-css' href='{{ asset('css/style.css') }}' type='text/css' media='screen, print'/>
    @yield('page-css')
    <style id='vce_style-inline-css' type='text/css'>
        body, button, input, select, textarea {
            font-size: 1.6rem;
        }

        .vce-single .entry-headline p {
            font-size: 2.2rem;
        }

        .main-navigation a {
            font-size: rem;
        }

        .sidebar .widget-title {
            font-size: 1.8rem;
        }

        .sidebar .widget {
            font-size: 1.4rem;
        }

        .vce-featured-link-article {
            font-size: 5.2rem;
        }

        .vce-featured-grid-big.vce-featured-grid .vce-featured-link-article {
            font-size: 3.4rem;
        }

        .vce-featured-grid .vce-featured-link-article {
            font-size: 2.2rem;
        }

        h1 {
            font-size: 4.5rem;
        }

        h2 {
            font-size: 3rem;
        }

        h3 {
            font-size: 2.8rem;
        }

        h4 {
            font-size: 2.5rem;
        }

        h5 {
            font-size: 2rem;
        }

        h6 {
            font-size: 1.8rem;
        }

        .comment-reply-title, .main-box-title {
            font-size: 2.2rem;
        }

        h1.entry-title {
            font-size: 3.3rem;
            line-height: 1.22;
            margin: 0 auto 15px;
            padding: 0;
            width: calc(100% - 60px) !important;
        }

        .vce-lay-a .entry-title a {
            font-size: 3.4rem;
        }

        .vce-lay-b .entry-title {
            font-size: 2.4rem;
        }

        .vce-lay-c .entry-title, .vce-sid-none .vce-lay-c .entry-title {
            font-size: 2.2rem;
        }

        .vce-lay-d .entry-title {
            font-size: 1.5rem;
        }

        .vce-lay-e .entry-title {
            font-size: 1.4rem;
        }

        .vce-lay-f .entry-title {
            font-size: 1.4rem;
        }

        .vce-lay-g .entry-title a, .vce-lay-g .entry-title a:hover {
            font-size: 3rem;
        }

        .vce-lay-h .entry-title {
            font-size: 2.4rem;
        }

        .entry-meta div, .entry-meta div a, .vce-lay-g .meta-item, .vce-lay-c .meta-item {
            font-size: 1.4rem;
        }

        .vce-lay-d .meta-category a, .vce-lay-d .entry-meta div, .vce-lay-d .entry-meta div a, .vce-lay-e .entry-meta div, .vce-lay-e .entry-meta div a, .vce-lay-e .fn, .vce-lay-e .meta-item {
            font-size: 1.3rem;
        }

        body {
            background-color: #f0f0f0;
        }

        body, .mks_author_widget h3, .site-description, .meta-category a, textarea {
            font-family: 'Poppins';
            font-weight: 400;
        }

        h1, h2, h3, h4, h5, h6, blockquote, .vce-post-link, .site-title, .site-title a, .main-box-title, .comment-reply-title, .entry-title a, .vce-single .entry-headline p, .vce-prev-next-link, .author-title, .mks_pullquote, .widget_rss ul li .rsswidget, #bbpress-forums .bbp-forum-title, #bbpress-forums .bbp-topic-permalink {
            font-family: 'Poppins';
            font-weight: 400;
        }

        .main-navigation a, .sidr a {
            font-family: 'Poppins';
            font-weight: 400;
        }

        .vce-single .entry-content, .vce-single .entry-headline, .vce-single .entry-footer {
            width: 760px;
        }

        .vce-lay-a .lay-a-content {
            width: 760px;
            max-width: 760px;
        }

        .vce-page .entry-content, .vce-page .entry-title-page {
            width: 600px;
        }

        .vce-sid-none .vce-single .entry-content, .vce-sid-none .vce-single .entry-headline, .vce-sid-none .vce-single .entry-footer {
            width: 750px;
        }

        .vce-sid-none .vce-page .entry-content, .vce-sid-none .vce-page .entry-title-page, .error404 .entry-content {
            width: 600px;
            max-width: 600px;
        }

        body, button, input, select, textarea {
            color: #444444;
        }

        h1, h2, h3, h4, h5, h6, .entry-title a, .prev-next-nav a, #bbpress-forums .bbp-forum-title, #bbpress-forums .bbp-topic-permalink, .woocommerce ul.products li.product .price .amount {
            color: #232323;
        }

        a, .entry-title a:hover, .vce-prev-next-link:hover, .vce-author-links a:hover, .required, .error404 h4, .prev-next-nav a:hover, #bbpress-forums .bbp-forum-title:hover, #bbpress-forums .bbp-topic-permalink:hover, .woocommerce ul.products li.product h3:hover, .woocommerce ul.products li.product h3:hover mark, .main-box-title a:hover {
            color: #cf4d35;
        }

        .vce-square, .vce-main-content .mejs-controls .mejs-time-rail .mejs-time-current, button, input[type="button"], input[type="reset"], input[type="submit"], .vce-button, .pagination-wapper a, #vce-pagination .next.page-numbers, #vce-pagination .prev.page-numbers, #vce-pagination .page-numbers, #vce-pagination .page-numbers.current, .vce-link-pages a, #vce-pagination a, .vce-load-more a, .vce-slider-pagination .owl-nav > div, .vce-mega-menu-posts-wrap .owl-nav > div, .comment-reply-link:hover, .vce-featured-section a, .vce-lay-g .vce-featured-info .meta-category a, .vce-404-menu a, .vce-post.sticky .meta-image:before, #vce-pagination .page-numbers:hover, #bbpress-forums .bbp-pagination .current, #bbpress-forums .bbp-pagination a:hover, .woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button, .woocommerce ul.products li.product .added_to_cart, .woocommerce #respond input#submit:hover, .woocommerce a.button:hover, .woocommerce button.button:hover, .woocommerce input.button:hover, .woocommerce ul.products li.product .added_to_cart:hover, .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt, .woocommerce #respond input#submit.alt:hover, .woocommerce a.button.alt:hover, .woocommerce button.button.alt:hover, .woocommerce input.button.alt:hover, .woocommerce span.onsale, .woocommerce .widget_price_filter .ui-slider .ui-slider-range, .woocommerce .widget_price_filter .ui-slider .ui-slider-handle, .comments-holder .navigation .page-numbers.current, .vce-lay-a .vce-read-more:hover, .vce-lay-c .vce-read-more:hover {
            background-color: #cf4d35;
        }

        #vce-pagination .page-numbers, .comments-holder .navigation .page-numbers {
            background: transparent;
            color: #cf4d35;
            border: 1px solid #cf4d35;
        }

        .comments-holder .navigation .page-numbers:hover {
            background: #cf4d35;
            border: 1px solid #cf4d35;
        }

        .bbp-pagination-links a {
            background: transparent;
            color: #cf4d35;
            border: 1px solid #cf4d35 !important;
        }

        #vce-pagination .page-numbers.current, .bbp-pagination-links span.current, .comments-holder .navigation .page-numbers.current {
            border: 1px solid #cf4d35;
        }

        .widget_categories .cat-item:before, .widget_categories .cat-item .count {
            background: #cf4d35;
        }

        .comment-reply-link, .vce-lay-a .vce-read-more, .vce-lay-c .vce-read-more {
            border: 1px solid #cf4d35;
        }

        .entry-meta div, .entry-meta-count, .entry-meta div a, .comment-metadata a, .meta-category span, .meta-author-wrapped, .wp-caption .wp-caption-text, .widget_rss .rss-date, .sidebar cite, .site-footer cite, .sidebar .vce-post-list .entry-meta div, .sidebar .vce-post-list .entry-meta div a, .sidebar .vce-post-list .fn, .sidebar .vce-post-list .fn a, .site-footer .vce-post-list .entry-meta div, .site-footer .vce-post-list .entry-meta div a, .site-footer .vce-post-list .fn, .site-footer .vce-post-list .fn a, #bbpress-forums .bbp-topic-started-by, #bbpress-forums .bbp-topic-started-in, #bbpress-forums .bbp-forum-info .bbp-forum-content, #bbpress-forums p.bbp-topic-meta, span.bbp-admin-links a, .bbp-reply-post-date, #bbpress-forums li.bbp-header, #bbpress-forums li.bbp-footer, .woocommerce .woocommerce-result-count, .woocommerce .product_meta {
            color: #9b9b9b;
        }

        .main-box-title, .comment-reply-title, .main-box-head {
            background: #ffffff;
            color: #232323;
        }

        .main-box-title a {
            color: #232323;
        }

        .sidebar .widget .widget-title a {
            color: #232323;
        }

        .main-box, .comment-respond, .prev-next-nav {
            background: #f9f9f9;
        }

        .vce-post, ul.comment-list > li.comment, .main-box-single, .ie8 .vce-single, #disqus_thread, .vce-author-card, .vce-author-card .vce-content-outside, .mks-bredcrumbs-container, ul.comment-list > li.pingback {
            background: #ffffff;
        }

        .mks_tabs.horizontal .mks_tab_nav_item.active {
            border-bottom: 1px solid #ffffff;
        }

        .mks_tabs.horizontal .mks_tab_item, .mks_tabs.vertical .mks_tab_nav_item.active, .mks_tabs.horizontal .mks_tab_nav_item.active {
            background: #ffffff;
        }

        .mks_tabs.vertical .mks_tab_nav_item.active {
            border-right: 1px solid #ffffff;
        }

        #vce-pagination, .vce-slider-pagination .owl-controls, .vce-content-outside, .comments-holder .navigation {
            background: #f3f3f3;
        }

        .sidebar .widget-title {
            background: #ffffff;
            color: #232323;
        }

        .sidebar .widget {
            background: #f9f9f9;
        }

        .sidebar .widget, .sidebar .widget li a, .sidebar .mks_author_widget h3 a, .sidebar .mks_author_widget h3, .sidebar .vce-search-form .vce-search-input, .sidebar .vce-search-form .vce-search-input:focus {
            color: #444444;
        }

        .sidebar .widget li a:hover, .sidebar .widget a, .widget_nav_menu li.menu-item-has-children:hover:after, .widget_pages li.page_item_has_children:hover:after {
            color: #cf4d35;
        }

        .sidebar .tagcloud a {
            border: 1px solid #cf4d35;
        }

        .sidebar .mks_author_link, .sidebar .tagcloud a:hover, .sidebar .mks_themeforest_widget .more, .sidebar button, .sidebar input[type="button"], .sidebar input[type="reset"], .sidebar input[type="submit"], .sidebar .vce-button, .sidebar .bbp_widget_login .button {
            background-color: #cf4d35;
        }

        .sidebar .mks_author_widget .mks_autor_link_wrap, .sidebar .mks_themeforest_widget .mks_read_more {
            background: #f3f3f3;
        }

        .sidebar #wp-calendar caption, .sidebar .recentcomments, .sidebar .post-date, .sidebar #wp-calendar tbody {
            color: rgba(68, 68, 68, 0.7);
        }

        .site-footer {
            background: #373941;
        }

        .site-footer .widget-title {
            color: #ffffff;
        }

        .site-footer, .site-footer .widget, .site-footer .widget li a, .site-footer .mks_author_widget h3 a, .site-footer .mks_author_widget h3, .site-footer .vce-search-form .vce-search-input, .site-footer .vce-search-form .vce-search-input:focus {
            color: #f9f9f9;
        }

        .site-footer .widget li a:hover, .site-footer .widget a, .site-info a {
            color: #cf4d35;
        }

        .site-footer .tagcloud a {
            border: 1px solid #cf4d35;
        }

        .site-footer .mks_author_link, .site-footer .mks_themeforest_widget .more, .site-footer button, .site-footer input[type="button"], .site-footer input[type="reset"], .site-footer input[type="submit"], .site-footer .vce-button, .site-footer .tagcloud a:hover {
            background-color: #cf4d35;
        }

        .site-footer #wp-calendar caption, .site-footer .recentcomments, .site-footer .post-date, .site-footer #wp-calendar tbody, .site-footer .site-info {
            color: rgba(249, 249, 249, 0.7);
        }

        .top-header, .top-nav-menu li .sub-menu {
            background: #3a3a3a;
        }

        .top-header, .top-header a {
            color: #ffffff;
        }

        .top-header .vce-search-form .vce-search-input, .top-header .vce-search-input:focus, .top-header .vce-search-submit {
            color: #ffffff;
        }

        .top-header .vce-search-form .vce-search-input::-webkit-input-placeholder {
            color: #ffffff;
        }

        .top-header .vce-search-form .vce-search-input:-moz-placeholder {
            color: #ffffff;
        }

        .top-header .vce-search-form .vce-search-input::-moz-placeholder {
            color: #ffffff;
        }

        .top-header .vce-search-form .vce-search-input:-ms-input-placeholder {
            color: #ffffff;
        }

        .header-1-wrapper {
            height: 70px;
            padding-top: 15px;
        }

        .header-2-wrapper, .header-3-wrapper {
            height: 70px;
        }

        .header-2-wrapper .site-branding, .header-3-wrapper .site-branding {
            top: 15px;
            left: 0px;
        }

        .site-title a, .site-title a:hover {
            color: #232323;
        }

        .site-description {
            color: #aaaaaa;
        }

        .main-header {
            background-color: #ffffff;
        }

        .header-bottom-wrapper {
            background: #fcfcfc;
        }

        .vce-header-ads {
            margin: -10px 0;
        }

        .header-3-wrapper .nav-menu > li > a {
            padding: 25px 15px;
        }

        .header-sticky, .sidr {
            background: rgba(255, 255, 255, 0.95);
        }

        .ie8 .header-sticky {
            background: #ffffff;
        }

        .main-navigation a, .nav-menu .vce-mega-menu > .sub-menu > li > a, .sidr li a, .vce-menu-parent {
            color: #4a4a4a;
        }

        .nav-menu > li:hover > a, .nav-menu > .current_page_item > a, .nav-menu > .current-menu-item > a, .nav-menu > .current-menu-ancestor > a, .main-navigation a.vce-item-selected, .main-navigation ul ul li:hover > a, .nav-menu ul .current-menu-item a, .nav-menu ul .current_page_item a, .vce-menu-parent:hover, .sidr li a:hover, .sidr li.sidr-class-current_page_item > a, .main-navigation li.current-menu-item.fa:before, .vce-responsive-nav {
            color: #cf4d35;
        }

        #sidr-id-vce_main_navigation_menu .soc-nav-menu li a:hover {
            color: #ffffff;
        }

        .nav-menu > li:hover > a, .nav-menu > .current_page_item > a, .nav-menu > .current-menu-item > a, .nav-menu > .current-menu-ancestor > a, .main-navigation a.vce-item-selected, .main-navigation ul ul, .header-sticky .nav-menu > .current_page_item:hover > a, .header-sticky .nav-menu > .current-menu-item:hover > a, .header-sticky .nav-menu > .current-menu-ancestor:hover > a, .header-sticky .main-navigation a.vce-item-selected:hover {
            background-color: #ffffff;
        }

        .search-header-wrap ul {
            border-top: 2px solid #cf4d35;
        }

        .vce-cart-icon a.vce-custom-cart span {
            background: #cf4d35;
            font-family: 'Poppins';
        }

        .vce-border-top .main-box-title {
            border-top: 2px solid #cf4d35;
        }

        .tagcloud a:hover, .sidebar .widget .mks_author_link, .sidebar .widget.mks_themeforest_widget .more, .site-footer .widget .mks_author_link, .site-footer .widget.mks_themeforest_widget .more, .vce-lay-g .entry-meta div, .vce-lay-g .fn, .vce-lay-g .fn a {
            color: #FFF;
        }

        .vce-featured-header .vce-featured-header-background {
            opacity: 0.5
        }

        .vce-featured-grid .vce-featured-header-background, .vce-post-big .vce-post-img:after, .vce-post-slider .vce-post-img:after {
            opacity: 0.5
        }

        .vce-featured-grid .owl-item:hover .vce-grid-text .vce-featured-header-background, .vce-post-big li:hover .vce-post-img:after, .vce-post-slider li:hover .vce-post-img:after {
            opacity: 0.8
        }

        .vce-featured-grid.vce-featured-grid-big .vce-featured-header-background, .vce-post-big .vce-post-img:after, .vce-post-slider .vce-post-img:after {
            opacity: 0.5
        }

        .vce-featured-grid.vce-featured-grid-big .owl-item:hover .vce-grid-text .vce-featured-header-background, .vce-post-big li:hover .vce-post-img:after, .vce-post-slider li:hover .vce-post-img:after {
            opacity: 0.8
        }

        #back-top {
            background: #323232
        }

        .sidr input[type=text] {
            background: rgba(74, 74, 74, 0.1);
            color: rgba(74, 74, 74, 0.5);
        }

        .meta-image:hover a img, .vce-lay-h .img-wrap:hover .meta-image > img, .img-wrp:hover img, .vce-gallery-big:hover img, .vce-gallery .gallery-item:hover img, .vce_posts_widget .vce-post-big li:hover img, .vce-featured-grid .owl-item:hover img, .vce-post-img:hover img, .mega-menu-img:hover img {
            -webkit-transform: scale(1.1);
            -moz-transform: scale(1.1);
            -o-transform: scale(1.1);
            -ms-transform: scale(1.1);
            transform: scale(1.1);
        }
        .main-header .header-3-wrapper .site-title, .main-header .header-3-wrapper .site-title a{
            line-height: 40px;
            font-size:38px;
        }
    </style>
    <script type='text/javascript' src='{{ asset('js/jquery.js') }}'></script>
    <script type='text/javascript' src='{{ asset('js/jquery-migrate.min.js') }}'></script>
    @yield('page-js')
</head>

<body class="post-template-default single single-post postid-256 single-format-standard chrome vce-sid-right">
<div id="vce-main">
    <header id="header" class="main-header">
        <div class="container header-main-area header-3-wrapper">
            <div class="vce-res-nav">
                <a class="vce-responsive-nav" href="#sidr-main"><i class="fa fa-bars"></i></a>
            </div>
            <div class="site-branding">
                <span class="site-title">
                    <a href="{{ route('index') }}" title="AmzReviews">AmzReviews</a>
                </span>
            </div>
            <nav id="site-navigation" class="main-navigation" role="navigation">
                <ul id="vce_main_navigation_menu" class="nav-menu">
                    <li id="menu-item-15" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-15"><a href="{{ route('index') }}">Home</a>
                    <li id="menu-item-3" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-3 vce-cat-7"><a href="{{ route('category','automotive-and-industrial') }}">Automative</a>
                    <li id="menu-item-4" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-4 vce-cat-6"><a href="{{ route('category','beauty-health-and-grocery') }}">Health</a>
                    <li id="menu-item-5" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-5 vce-cat-10"><a href="{{ route('category','books-and-audible') }}">Books</a>
                    <li id="menu-item-6" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-6 vce-cat-8"><a href="{{ route('category','clothing-shoes-and-jewelry') }}">Fashion</a>
                    <li id="menu-item-7" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-7 vce-cat-4"><a href="{{ route('category','electronics-and-computers') }}">Computers</a>
                    <li id="menu-item-8" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-8 vce-cat-2"><a href="{{ route('category','home-garden-and-tools') }}">Home & Tools</a>
                    <li id="menu-item-10" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-10 vce-cat-3"><a href="{{ route('category','sports-and-outdoors') }}">Sports</a>
                    <li id="menu-item-11" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-11 vce-cat-5"><a href="{{ route('category','toys-kids-and-baby') }}">Kids</a>
                    <li class="search-header-wrap"><a class="search_header" href="javascript:void(0)"><i class="fa fa-search"></i></a>
                        <ul class="search-header-form-ul">
                            <li>
                                <form class="search-header-form" action="#" method="get">
                                    <input name="s" class="search-input" size="20" type="text" value="Type here to search..." onfocus="(this.value == 'Type here to search...') && (this.value = '')" onblur="(this.value == '') && (this.value = 'Type here to search...')" placeholder="Type here to search..."/>
                                </form>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
    </header>

    <div id="main-wrapper">
        <div id="content" class="container site-content vce-sid-right">
            <div id="primary" class="vce-main-content">
                @yield('content')
            </div>

            <aside id="sidebar" class="sidebar right">
                <div id="search-2" class="widget widget_search">
                    <form class="vce-search-form" action="#" method="get">
                        <input name="s" class="vce-search-input" size="20" type="text" value="Type here to search..."
                               onfocus="(this.value == 'Type here to search...') && (this.value = '')"
                               onblur="(this.value == '') && (this.value = 'Type here to search...')"
                               placeholder="Type here to search..."/>
                        <button type="submit" class="vce-search-submit"><i class="fa fa-search"></i></button>
                    </form>
                </div>
                <?php
                    $recent_posts=\DB::table("lists")
                    ->select("lists.*")->where('lists.status',1)
                    ->orderBy("lists.created_at","desc")->limit(15)->get();
                ?>
                <div id="recent-posts-2" class="widget widget_recent_entries"><h4 class="widget-title">Recent Post</h4>
                    @if(count($recent_posts)>0)
                        <ul>
                            @foreach($recent_posts as $row)
                                <li>
                                    <a href="{{ route('index',$row->slug) }}">Top 10 {{ $row->meta_title.' '.date('Y') }}</a>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </div>
            </aside>
        </div>

        <footer id="footer" class="site-footer">
            <div class="container-full site-info">
                <div class="container">
                    <div class="vce-wrap-left">
                        <p></p>
                    </div>
                    <div class="vce-wrap-right">
                        <ul id="vce_footer_menu" class="bottom-nav-menu">
                            <li id="menu-item-159" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-159"><a href="{{ route('page','about-us') }}">About Us</a></li>
                            <li id="menu-item-158" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-158"><a href="{{ route('page','privacy-policy') }}">Privacy Policy</a></li>
                            <li id="menu-item-158" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-158"><a href="{{ route('page','cookie-policy') }}">Cookie Policy</a></li>
                            <li id="menu-item-158" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-158"><a href="{{ route('page','affiliate-disclosure') }}">Amazon Associate Disclosure</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>
<a href="javascript:void(0)" id="back-top"><i class="fa fa-angle-up"></i></a>

<script type='text/javascript'>
    /* <![CDATA[ */
    var vce_js_settings = {
        "sticky_header": "1",
        "sticky_header_offset": "700",
        "sticky_header_logo": "",
        "logo": "",
        "logo_retina": "",
        "logo_mobile": "",
        "logo_mobile_retina": "",
        "rtl_mode": "0",
        "ajax_url": "",
        "ajax_mega_menu": "1",
        "mega_menu_slider": "",
        "mega_menu_subcats": "",
        "lay_fa_grid_center": "",
        "full_slider_autoplay": "",
        "grid_slider_autoplay": "",
        "grid_big_slider_autoplay": "",
        "fa_big_opacity": {"1": "0.5", "2": "0.7"},
        "top_bar_mobile": "1",
        "top_bar_mobile_group": "",
        "top_bar_more_link": "More"
    };
    /* ]]> */
</script>
<script type='text/javascript' src='{{ asset('js/min.js') }}'></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-132782018-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-132782018-1');
</script>
</body>
</html>