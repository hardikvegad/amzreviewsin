@extends('layout')
@section('meta')
    <title>Amazon Associate Disclosure - AmzReviews</title>
@stop
@section('content')
    <main id="main" class="main-box main-box-single">
        <article class="vce-single post type-post status-publish format-standard has-post-thumbnail hentry">
            <header class="entry-header">
                <h1 class="entry-title">Amazon Associate Disclosure</h1>
            </header>
            <div class="entry-content ">
                <p>amzreviews.in is a participant in the Amazon Services LLC Associates Program, an affiliate advertising program designed to provide a means for sites to earn advertising fees by advertising and linking <a href="{{ route('index') }}"></a> https://AmzReviews.in to Amazon properties.</p>
            </div>
        </article>
    </main>
@stop